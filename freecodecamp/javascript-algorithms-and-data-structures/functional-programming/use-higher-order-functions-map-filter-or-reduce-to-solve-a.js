const squareList = (arr) => {
  // only change code below this line
  let nums = arr.filter(function(item) {
   return item > 0 && item % parseInt(item) === 0;    
  });
    arr = nums.map(function(num) {
        return Math.pow(num, 2);
    })
    return arr;
  // only change code above this line
};

// test your code
const squaredIntegers = squareList([-3, 4.8, 5, 3, -3.2]);
console.log(squaredIntegers);
