console.group("Topic: Promises");
// Task 01
// Создайте промис, который постоянно находиться в состоянии pending.
// В конструкторе промиса выведите в консоль сообщение "Promise is created".

const promise01 = new Promise((res, rej) => console.log('Promise is created'));

console.log(promise01);

// Task 02
// Создайте промис, который после создания сразу же переходит в состояние resolve
// и возвращает строку 'Promise Data'
// Получите данные промиса и выведите их в консоль

const promise02 = new Promise((resolve, reject) => resolve('Promise Data'));         

promise02.then(data => console.log(data));
console.log(promise02);

// Task 03
// Создайте промис, который после создания сразу же переходит в состояние rejected
// и возвращает строку 'Promise Error'
// Получите данные промиса и выведите их в консоль

const promise03 = new Promise((res, rej) => rej('Promise Error'));

promise03.then(data => console.log(data));
console.log(promise03);

// Task 04
// Создайте промис, который переходит в состояние resolved через 3с.
// (Используйте setTimeout) и возвращает строку 'Promise Data'
// Получите данные промиса и выведите их в консоль

const promise04 = new Promise((res, rej) => {
     setTimeout(() => {
        res('Promise Data'); 
     },3000)
     ;
});

promise04.then(data => console.log(data));

console.log(promise04);

// Task 05
// Создайте литерал объекта handlePromise со следующими свойствами:
// promise, resolve, reject, onSuccess, onError
// Проинициализируйте первые три свойства null,
// а последние два функциями, которые принимают один параметр и выводят
// в консоль сообщения: первая - `Promise is resolved with data: ${paramName}`
// вторая - `Promise is rejected with error: ${paramName}`
// Создайте три кнопки и три обработчика события click для этих кнопок
// Первый обработчик, создает промис, заполняет первые три свойства,
// описаного выше объекта: свойство promise получает новый созданный промис,
// свойства resolve и reject получают ссылки на соответствующие функции
// resolve и reject. Следующие два обработчика запускают методы resolve и reject.

const handlePromise = {
    promise: null,
    resolve: null,
    reject: null,
    onSuccess: function(paramName) {
        console.log(`Promise is resolved with data: ${paramName}`);
    },
    onError: function(paramName) {
        console.log(`Promise is rejected with error: ${paramName}`);
    }
};

const btnCreate = document.querySelector('#btn-create-promise');
const btnResolve = document.querySelector('#btn-resolve-promise');
const btnReject = document.querySelector('#btn-reject-promise');

btnCreate.addEventListener('click', createPromise);
btnResolve.addEventListener('click', getResolve);
btnReject.addEventListener('click', getReject);

function createPromise() {
    const promise05 = new Promise((res, rej) => {
        handlePromise.resolve = res;
        handlePromise.reject = rej;
    })

    handlePromise.promise = promise05;
}

function getResolve() {
    handlePromise.resolve(1);
}

function getReject() {
    handlePromise.reject(2);
}

// Task 06
// Используйте предыдущее задание. Продублируйте строчку с методом then

// Task 07
// Создайте промис, который через 1 с возвращает строку "My name is".
// Создайте функцию onSuccess, которая получает один параметр,
// прибавляет к нему Ваше имя и возвращает новую строку из функции
// Создайте функцию print, которая выводит в консоль значение своего параметра
// Добавьте два метода then и зарегистрируйте созданные функции.

const promise07 = new Promise((res, rej) => {
    setTimeout(() => {
       res('My name is'); 
    },1000);
})

console.log(promise07);

function onSuccess(par) {
    return `${par} Roman`;
}

function print(par) {
    console.log(par);
}

promise07.then(onSuccess).then(print);

// Task 08
// Используйте предыдущий код. Добавьте в функци onSuccess генерацию исключения
// Обработайте даное исключение, используя catch. Обратите внимание,
// что метод print при этом не выполняется.

// Task 09
// Напишите функцию getPromiseData, которая принимает один параметр - промис. Функция получает
// значение промиса и выводит его в консоль
// Объявите объект со свойтвом name и значением Anna.
// Создайте врапер для этого объекта и вызовите для него функцию getPromiseData

function getPromiseData(prom) {
    prom.then((data) => {
        console.log(data);
    });
}

const promise09 = new Promise((res, rej) => {
   res({name: 'Anna'})
});

getPromiseData(promise09);


// Task 10
// Создайте два промиса. Первый промис возвращает объект { name: "Anna" } через 2с,
// а второй промис возвращает объект {age: 16} через 3 с.
// Получите результаты работы двух промисов, объедините свойства объектов
// и выведите в консоль

const prom10 = new Promise((res, rej) => {
    setTimeout(() => {
        res({name: "Anna"});
    }, 2000)
});

prom10.then(res => console.log(res));

const prom10_1 = new Promise((res, rej) => {
    setTimeout(() => {
        res({age: 16});
    }, 3000)
});

prom10_1.then(res => console.log(res));


Promise.all([prom10, prom10_1]).then(val => console.log(val));


// Task 11
// Используйте предыдущее задание. Пусть теперь второй промис переходит в
// состояние rejected со значением "Promise Error". Измените код, чтобы обработать
// эту ситуацию.

const prom11 = new Promise((res, rej) => {
    setTimeout(() => {
        res({name: "Anna"});
    }, 2000)
});

prom11.then(res => console.log(res));

const prom11_1 = new Promise((res, rej) => {
    setTimeout(() => {
        rej('Promise Error');
    }, 3000)
});

prom11_1.then(res => console.log(res)).catch(rej => console.log(rej));


Promise.all([prom11, prom11_1]).then(val => console.log(val)).catch(val => console.log(val));

// Task 12
// Создайте промис, который перейдет в состояние resolve через 5с и вернет строку
// 'Promise Data'.
// Создайте второй промис, который перейдет в состояние rejected по клику на
// кнопку. Добавьте обработчик для кнопки.
// Используя метод race организуйте отмену промиса.

const btnCancel = document.querySelector('#btn-cancel-promise');


const prom12_1 = new Promise((res, rej) => {
    setTimeout(() => {
        res('Promise Data');
    }, 5000)
});

prom12_1.then(res => console.log(res));
const prom12_2 = new Promise((res, rej) => {
    rej(btnCancel.addEventListener('click', getCancel));
})

function getCancel() {
    prom12_2.catch(val => val)
}

console.groupEnd();