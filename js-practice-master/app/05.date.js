console.group("Topic: Date object");

// Task 1
// RU: Создать текущую дату и вывести ее в формате dd.mm.yyyy и dd Month yyyy
// EN: Create current date and display it in the console according to the format
//     dd.mm.yyyy и dd Month yyyy
const todayDate = new Date;
console.log(todayDate.toLocaleDateString());

const nowDay = todayDate.getDate();
const nowYear = todayDate.getFullYear();
const month = new Array(12);
month[0]="Января";
month[1]="Февраля";
month[2]="Марта";
month[3]="Апреля";
month[4]="Мая";
month[5]="Июня";
month[6]="Июля";
month[7]="Августа";
month[8]="Сентября";
month[9]="Октября";
month[10]="Ноября";
month[11]="Декабря";
const nowMonth = month[todayDate.getMonth()];
console.log(`${nowDay} ${nowMonth} ${nowYear}`);

// Task 2
// RU: Создать объект Date из строки '15.03.2025'.
// EN: Create an object Date from the string '15.03.2025'.

const date2 = '15.03.2025';
date2 = date2.split('.');
[date2[0], date2[1]] = [date2[1], date2[0]];
date2 = date2.join('/');

const newDate = new Date(date2);
console.log(newDate);

// Task 3
// RU: Создать объект Date, который содержит:
//     1. завтрашнюю дату,
//     2. первое число текущего месяца,
//     3. последнее число текущего месяца
// EN: Create an object Date, which represents:
//     1. tomorrow
//     2. first day of the current month
//     3. last day of the current month

const justNow = new Date();
console.log(justNow);

const tomorrowDate = new Date(new Date().setDate(new Date().getDate() +1));

console.log(tomorrowDate);

const firstDayInThisMonth = new Date().getDate() - (new Date().getDate() - 1);
console.log(firstDayInThisMonth);

const lastDayInThisMonth = 33 - (new Date(new Date().getFullYear(), new Date().getMonth(), 33)).getDate();
console.log(lastDayInThisMonth);

// Task 4
// RU: Подсчитать время суммирования чисел от 1 до 1000.
// EN: Calculate the time of summing numbers from 1 to 1000.

const startDate = Date.now();
console.log(startDate);

function sum(num) {
    let result = 0;
    for (let i = 1; i <= num; i++) {
        result += i;
   }
    
    return result;
}

sum(1000);

const endDate = Date.now();
console.log(endDate);

const result = endDate - startDate;
console.log(`время суммирования: ${result} милисекунд`);



// Task 5
// RU: Подсчитать количество дней с текущей даты до Нового года.
// EN: Calculate the number of days from the current date to the New Year.



const timeend = new Date();
timeend = new Date(timeend.getFullYear() + 1,0,1).getTime();

const justNow = new Date();
justNow = justNow.getTime();

const daysUntilNewYear = Math.floor((timeend - justNow) / 86400000);

console.log(timeend);
console.log(justNow);
console.log(`До Нового Года осталось: ${daysUntilNewYear} дней(дня)`);

console.groupEnd();
