console.group("Topic: DOM");

// Task 01
// Найти элемент с id= "t01". Вывести в консоль.
// Найти родительский элемент и вывести в консоль.
// Найти дочерние ноды, если они есть, и вывести в консоль
// названия и тип нод.

const t01 = document.getElementById('t01');
console.log(t01);
const parent01 = t01.parentNode;
console.log(parent01);
const child01 = t01.childNodes;
console.log(child01);
const nemeNodes = child01.forEach((child) => {
    console.log(`${child.nodeType}: ${child.nodeName}`);
    
});


// Task 02
// Подсчитать количество <li> элементов на странице. Для поиска элементов использовать
// getElementsByTagName(). Вывести в консоль.
// Добавить еще один элемент в список и вывести снова их количество.


getSumEl('li');

const newLi = document.createElement('li');

const ul = document.querySelectorAll('ul');


ul.forEach(el =>  el.insertAdjacentHTML('beforeend', '<li>Item4</li>'));      

getSumEl('li');

function getSumEl(el) {
   const sum = document.getElementsByTagName(el).length;
   console.log(sum);
}


// Task 03
// Получить элементы <li> используя метод querySelectorAll() и вывети их в консоль
// Добавить новый <li> и снова вывести в консоль

getElements('#t01 li');

const ul03 = document.querySelector('#t01 ul');
console.log(ul03);

ul03.insertAdjacentHTML('beforeend', '<li>Item5</li>');
getElements('#t01 li');

function getElements(selector) {
    const elements = document.querySelectorAll(selector);
    console.log(elements);
}

// Task 04
// Найти все первые параграфы в каждом диве и установить цвет фона #ffff00

const firstP = document.querySelectorAll('.t04 p:first-of-type');
console.log(firstP);
firstP.forEach(p => p.style.backgroundColor = '#ffff00');

// Task 05
// Подсчитать сумму строки в таблице и вывести ее в последнюю ячейку

let tdList = document.querySelectorAll('td');

function getSum(arr) {
    let result = 0;
    for( let i = 0; i < arr.length; i++) {
        result = result + Number(arr[i].textContent);
        roundModResult = Math.round(result / 0.1) * 0.1;
    }

    return roundModResult;
}

const sum = getSum(tdList);

const lastTd = document.querySelector('td:last-of-type');

lastTd.innerHTML = sum;

// Task 06
// Вывести значения всех атрибутов элемента с идентификатором t06

const t06Attr = document.getElementById('t06').attributes;

for (let i = 0; i < t06Attr.length; i++) {
    console.log(t06Attr[i]);
}

// Task 07
// Получить объект, который описывает стили, которые применены к элементу на странице
// Вывести объект в консоль. Использовать window.getComputedStyle().

const el07 = document.querySelector('#btn');

const styles = window.getComputedStyle(el07);
console.log(styles);

// Task 08
// Установите в качестве контента элемента с идентификатором t08 следующий параграф
// <p>This is a paragraph</>

const el08 = document.querySelector('#t08');

el08.innerHTML = '<p>This is a paragraph</p>';

// Task 09
// Создайте элемент <div class='c09' data-class='c09'> с некоторым текстовым контентом, который получить от пользователя,
// с помощью prompt, перед элементом с идентификатором t08,
// когда пользователь кликает на нем

const textCont = prompt('yuor text');

const t08 = document.querySelector('#t08');
const newDiv = document.createElement('div');

newDiv.className = 'c09';
newDiv.setAttribute('data-class', 'c09');

t08.addEventListener('click', divClick); 
    
function divClick() {
    t08.before(newDiv);
    newDiv.innerHTML = textCont;
}

// Task 10
// Удалите у элемента с идентификатором t06 атрибут role
// Удалите кнопку с идентификатором btn, когда пользователь кликает по ней

const elT06 = document.querySelector('#t06');
elT06.removeAttribute('role');

let btng = document.querySelector('#btn');
btn.addEventListener('click', btnClick);

function btnClick() {
    btn.parentNode.removeChild(btn);
}

console.groupEnd();