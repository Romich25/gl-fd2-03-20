console.group("Topic: Strings");

// Task 01. padStart
// RU: Объявите три переменных: hour, minute, second.
//     Присвойте им следующие значения: 4, 35, 5.
//     Выведите в консоль время в формате 04:35:05.
// EN: Declare three variables: hour, minute, second.
//     Assign them the following values: 4, 35, 5.
//     Display the time in the format 04:35:05 in the console.
let hour = 4;
let minute = 35;
let second = 5;

function getDate() {
    if (hour < 10) {
         hour = '0' + hour;
    }
    if (minute < 10) {
        minute = '0' + minute;
    } 
    if (second < 10) {
        second = '0' + second;
    }

    return `${hour}:${minute}:${second}`;
}

console.log(getDate());

// Task 02. repeat
// RU: Создайте функцию, которая выведет в консоль пирамиду на 9 уровней как показано ниже
//     1
//     22
//     333
//     4444
//     ...
// EN: Create a function which displays a 9 level pyramid in the console according to the
//     following pattern
//     1
//     22
//     333
//     4444
//     ...
function generatePyramid(num) {
  let number = '';

  for (let i = 1; i <= num; i++) {
    console.log(String(i).repeat(i));
  }
}
generatePyramid(9);

// Task 03. includes
// RU: Напишите код, который выводит в консоль true, если строка str содержит
//     'viagra' или 'XXX', а иначе false.
//     Тестовые данные: 'buy ViAgRA now', 'free xxxxx'
// EN: Create a snippet of code which displays the value true in the console
//     when str contains 'viagra' or 'XXX', otherwise it displays false.

function inc(str) {
    if (str.includes('viagra') || str.includes('XXX')) {
        console.log(true);
    } else {
        console.log(false);
    }
}

inc('buy ViAgRA now');
inc('free xxxxx');

// Task 04. includes + index
// RU: Проверить, содержит ли строка второе вхождение подстроки,
//     вернуть true/false.
// EN: Check whether the string contains a second occurrence of a substring,
//     return true / false.

// Task 05. Template literal
// RU: Создать строку: "ten times two totally is 20"
//     используя переменные:
//     const a = 10;
//     const b = 2;
//     и template literal
// EN: Create s string "ten times two totally is 20"
//     using the following variables:
//     const a = 10;
//     const b = 2;
//     and template literal
const a = 10;
const b = 2;
const str5 = `ten times two totally is ${a * b}`;
console.log(str5);

// Task 06. normalize
// RU: Создайте функцию, которая сравнивает юникод строки.
//     Сравните две строки
//     var str1 = '\u006d\u0061\u00f1';
//     var str2 = '\u006d\u0061\u006e\u0303';
// EN: Create a function that compares the unicode strings.
//     Compare 2 strings:
//     var str1 = '\u006d\u0061\u00f1';
//     var str2 = '\u006d\u0061\u006e\u0303';

const str1 = '\u006d\u0061\u00f1';
const str2 = '\u006d\u0061\u006e\u0303';

function compareStr(st1, st2) {
    if(str1.normalize() === str2.normalize()) {
        return 'строки равны';
    }
    else {
        return 'строки не равны';
    }
}

console.log(compareStr(str1, str2));


// Task 07. endsWith
// RU: Создайте функцию, которая на вход получает массив имен файлов и расширение файла
//     и возвращает новый массив, который содержит файлы указанного расширения.
// EN: Create a function that gets an array of file names and a file extension as its parameters
//     and returns a new array that contains the files of the specified extension.

function getNewArr(arr, ex) {
   return arr.map(el => `${el}.${ex}`)
}

const arrName = ['cat', 'dog', 'pig'];
console.log(getNewArr(arrName, 'jpg'));

// Task 08. String.fromCodePoint
// RU: Создать функцию, которая выводит в консоль строчку в формате 'символ - код'
//     для кодов в диапазоне 78000 - 80000.
// EN: Create a function that displays a line in the format 'character - code' to the console
//     for codes in the range of 78000 - 80000.

function getSimbolCode(start, end) {
   for (let i = start; i <= end; i++) {
//        console.log(i + '-' + String.fromCodePoint(i));
       console.log(`${String.fromCodePoint(i)} - ${i}`);
    }
}

getSimbolCode(78000, 80000);

// Task 09
// RU: Создайте функцию, которая должна выводить в консоль следующую пирамиду
//     Пример:
//     pyramid(1) = '#'
//
//     pyramid(2) = ' # '
//                  '###'
//
//     pyramid(3) = '  #  '
//                  ' ### '
//                  '#####'
// EN: Create a function that should display the next pyramid in the console
//     Example:
//     pyramid(1) = '#'
//
//     pyramid(2) = ' # '
//                  '###'
//
//     pyramid(3) = '  #  '
//                  ' ### '
//                  '#####'

function pyramid(num) {

   for(let i=1; i<= num; i++){

    let str = ' '.repeat(num-i);
    let str2 = '#'. repeat(i*2 -1);

   console.log(str + str2 + str);
  }
}

pyramid(4);

// Task 10
// RU: Создайте тег-функцию currency, которая формитирует числа до двух знаков после запятой
//     и добавляет знак доллара перед числом в шаблонном литерале.
// EN: Create a currency tag function that forms numbers up to two decimal digits.
//     and adds a dollar sign before the number in the template literal.

console.groupEnd();
