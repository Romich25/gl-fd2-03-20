console.group("Topic: Primitive Data Types");

// Task 01
// Объявите переменную days и проинициализируйте ее числом от 1 до 10.
// Преобразуйте это число в количество секунд и выведите в консоль.

const days = 9;
const secondsOnDay = days * 24 * 60 * 60;
console.log(secondsOnDay);

// Task 02
// Объявите две переменные: admin и name. Установите значение переменной name
// в ваше имя. Скопируйте это значение в переменную admin и выведите его в консоль.

let admin;
lett name;
name = 'Roman';
admin = name;
console.log(admin);

// Task 03
// Объявите три переменных: a, b, c. Присвойте им следующие значения: 10, 2, 5.
// Объявите переменную result1 и вычислите сумму значений переменных a, b, c.
// Объявите переменную min и вычислите минимальное значение переменных a, b, c.
// Выведите результат в консоль.

let a;
let b;
let c;
a = 10;
b = 2;
c = 5;
let result = a + b+ c;
console.log(result);
let min = Math.min(a, b, c);
console.log(min);

// Task 04
// Объявите три переменных: hour, minute, second. Присвойте им следующие значения:
// 10, 40, 25. Выведите в консоль время в формате 10:40:25.
let hour;
let minute;
let second;

hour = 10;
minute = 40;
second = 25;

console.log(`${hour}:${minute}:${second}`);

// Task 05
// Объявите переменную minute и проинициализируйте ее целым числом.
// Вычислите к какой четверти принадлежит это число и выведите в консоль.

const minute05 = 24;


switch (true) {
    case minute05 <= 15:
        console.log('первая четверть часа');
        break;
    case minute05 > 15 && minute05 <= 30:
        console.log('вторая четверть часа');
        break;
    case minute05 > 30 && minute05 <= 45:
        console.log('третья четверть часа');
        break;
    case minute05 > 45 && minute05 <= 60:
        console.log('четверная четветь часа'); 
        break;
    default:
        console.log('введите число не более 60');
}
// Task 06
// Объявите две переменные, которые содержат стоимость товаров:
// первый товар - 0.10 USD, второй - 0.20 USD
// Вычислите сумму и выведите в консоль. Используйте toFixed()

const price1 = '0.10 USD';
const price2 = '0.20 USD';

let summa = (+price1.replace('USD', '') + +price2.replace('USD', '')).toFixed(2);
console.log(summa);

// Task 07
// Объявите переменную a.
// Если значение переменной равно 0, выведите в консоль "True", иначе "False".
// Проверьте, что будет появляться в консоли для значений 1, 0, -3.

const a = 1;


if (a === 0) {
    console.log('True')
} else {
    console.log('False')
}


// Task 08
// Объявите две переменных: a, b. Вычислите их сумму и присвойте переменной result.
// Если результат больше 5, выведите его в консоль, иначе умножте его на 10
// и выведите в консоль.
// Данные для тестирования: 2, 5 и 3, 1.

const a = 3;
const b = 5;
const result = a + b;

if (result > 5) {
    console.log(result);
} else {
    console.log(result * 10);
}

// Task 09
// Объявите переменную month и проинициализируйте ее числом от 1 до 12.
// Вычислите время года и выведите его в консоль.

 const month = 12;

switch(true) {
    case month < 3 || month === 12:
        console.log('winter');
        break;
    case month >= 3 && month  <= 5:
        console.log('spring');
        break;
    case month >= 6 && month <= 8:
        console.log('summer');
        break;
    case month >= 9 && month <= 11:
        console.log('autumn');
        break;
    default:
        console.log('error');
} 

// Task 10
// Выведите в консоль все числа от 1 до 10.

for (let num = 1; num <= 10; num++) {
    console.log(num);
}

// Task 11
// Выведите в консоль все четные числа от 1 до 15.



for ( let evNum = 2; evNum < 15; evNum += 2) {
    console.log(evNum);
}

// Task 12
// Нарисуйте в консоле пирамиду на 10 уровней как показано ниже
// x
// xx
// xxx
// xxxx
// ...

for (let i = 0; i <= 10; i++) {
    console.log('x'.repeat(i));
}

// Task 13
// Нарисуйте в консоле пирамиду на 9 уровней как показано ниже
// 1
// 22
// 333
// 4444
// ...

for (let i = 0; i <= 9; i++) {
    console.log(`${i}`.repeat(i));
}

// Task 14
// Запросите у пользователя какое либо значение и выведите его в консоль.

const userName = prompt('введите ваше имя');
console.log(userName);


// Task 15
// Перепишите if используя тернарный опертор
// if (a + b < 4) {
//   result = 'Мало';
// } else {
//   result = 'Много';
// }

a + b < 4 ? 'Мало' : 'Много';

// Task 16
// Перепишите if..else используя несколько тернарных операторов.
// var message;
// if (login == 'Вася') {
//   message = 'Привет';
// } else if (login == 'Директор') {
//   message = 'Здравствуйте';
// } else if (login == '') {
//   message = 'Нет логина';
// } else {
//   message = '';
// }

(login == 'Вася') ? message = 'Привет' : 
(login == 'Директор') ? message = 'Здравствуйте' : 
(login == '') ? message = 'Нет логина' : message = '';

// Task 17
// Замените for на while без изменения поведения цикла
// for (var i = 0; i < 3; i++) {
//   alert( "номер " + i + "!" );
// }

let i = 0;
while (i < 3) {
    alert( "номер " + i + "!" );
    i++;
}

// Task 18
// Напишите цикл, который предлагает prompt ввести число, большее 100.
// Если пользователь ввёл другое число – попросить ввести ещё раз, и так далее.
// Цикл должен спрашивать число пока либо посетитель не введёт число,
// большее 100, либо не нажмёт кнопку Cancel (ESC).
// Предусматривать обработку нечисловых строк в этой задаче необязательно.

let num = prompt('введите число большее 100');
while (num < 100) {
    num = prompt('введите число большее 100');
}
console.log(num);

// Task 19
// Переписать следующий код используя switch
// var a = +prompt('a?', '');
// if (a == 0) {
//   alert( 0 );
// }
// if (a == 1) {
//   alert( 1 );
// }
// if (a == 2 || a == 3) {
//   alert( '2,3' );
// }

const a = +prompt('a?', '');

switch(true) {
    case a == 0:
        alert (0);
        break;
    case a == 1:
        alert(1);
        break;
    case a == 2 || a == 3:
        alert('2,3');
        break;
}

// Task 20
// Объявите переменную и проинициализируйте ее строчным значением в переменном
// регистре. (Например так "таООооОддОО")
// Напишите код, который преобразует эту строку к виду:
// первая буква в верхнем регистре, остальные буквы в нижнем регистре.
// Выведите результат работы в консоль
// Используйте: toUpperCase/toLowerCase, slice.

const str = 'ПРивЕТ';
function getStr() {
    return str.toUpperCase().slice(0, 1) + str.toLocaleLowerCase().slice(1, str.length);
}

getStr(str);

// Task 21
// Напишите код, который выводит в консоль true, если строка str содержит
// „viagra“ или „XXX“, а иначе false.
// Тестовые данные: 'buy ViAgRA now', 'free xxxxx'

function inc(str) {
    if (str.includes('viagra') || str.includes('XXX')) {
        console.log(true);
    } else {
        console.log(false);
    }
}

inc('buy ViAgRA now');
inc('free xxxxx');

// Task 22
// Напишите код, который проверяет длину строки str, и если она превосходит
// maxlength – заменяет конец str на "...", так чтобы ее длина стала равна maxlength.
// Результатом должна быть (при необходимости) усечённая строка.
// Выведите строку в консоль
// Тестовые данные:
//  "Вот, что мне хотелось бы сказать на эту тему:", 20
//  "Всем привет!", 20

function getDots(str, len) {
    if (str.length > len) {
        return str.substr(0, len - 3) + '...';
    } else {
        return str;
    }
   
}

console.log(getDots("Вот, что мне хотелось бы сказать на эту тему:", 20));
console.log(getDots("Всем привет!", 20));

// Task 23
// Напишите код, который из строки $100 получит число и выведите его в консоль.

function getDolNum(dol) {
    return +dol.slice(1, dol.length);
}

console.log(getDolNum('$100'));

// Task 24
// Напишите код, который проверит, является ли переменная промисом

console.groupEnd();
